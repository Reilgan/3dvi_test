import asyncio
from datetime import datetime
import json
import logging

from aio_pika.abc import AbstractIncomingMessage

from rabbit_broker import RabbitConnection
from settings import RABBITMQ_DEFAULT_USER, RABBITMQ_DEFAULT_PASS, RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_EXCHANGE, \
    RABBITMQ_OUT_QUEUE, RABBITMQ_IN_QUEUE

rabbit_broker = RabbitConnection()

logger = logging.getLogger(__name__)


async def pause_job(sec: int) -> bool:
    await asyncio.sleep(sec)
    return True


async def process_message(
    message: AbstractIncomingMessage,
) -> None:
    async with message.process():
        message_dict = json.loads(message.body)
        if await pause_job(message_dict['delay']):
            message_dict['write_time'] = str(datetime.utcnow())
            await rabbit_broker.send_messages(
                [message_dict]
            )
        else:
            raise Exception()


async def main():
    await rabbit_broker.connect(
        f"amqp://{RABBITMQ_DEFAULT_USER}:{RABBITMQ_DEFAULT_PASS}@{RABBITMQ_HOST}:{RABBITMQ_PORT}/",
        RABBITMQ_EXCHANGE,
        in_queue=RABBITMQ_IN_QUEUE,
        out_queue=RABBITMQ_OUT_QUEUE
    )

    await rabbit_broker.add_consumer(process_message)
    # try:
    while True:
        await rabbit_broker.consume()
    # except Exception as e:
    #     print(e)
    #     await main()


if __name__ == "__main__":
    asyncio.run(main())
