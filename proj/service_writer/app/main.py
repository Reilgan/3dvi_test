import asyncio
from datetime import datetime
import json
import logging
import os.path

from aio_pika.abc import AbstractIncomingMessage

from rabbit_broker import RabbitConnection
from settings import RABBITMQ_DEFAULT_USER, RABBITMQ_DEFAULT_PASS, RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_EXCHANGE, \
    RABBITMQ_IN_QUEUE

rabbit_broker = RabbitConnection()

logger = logging.getLogger(__name__)


async def pause_job(sec: int) -> bool:
    await asyncio.sleep(sec)
    return True


def write_1_file(message_dict):
    write_header_1 = False
    if os.path.exists('app/1.txt') is False:
        write_header_1 = True

    with open('app/1.txt', 'a', newline='') as file:
        if write_header_1:
            file.write(f"id\t|\trecieve_time\t|\twrite_time\n")
        file.write(f"{message_dict['id']}\t|\t{message_dict['recieve_time']}\t|\t{message_dict['write_time']}\n")


def write_2_file(message_dict):
    write_header_2 = False
    if os.path.exists('app/2.txt') is False:
        write_header_2 = True

    with open('app/2.txt', 'a', newline='') as file:
        if write_header_2:
            file.write(f"id\t|\trecieve_time\n")
        file.write(f"{message_dict['id']}\t|\t{message_dict['recieve_writer_time']}\n")


async def process_message(
    message: AbstractIncomingMessage,
) -> None:
    async with message.process():
        message_dict = json.loads(message.body)
        message_dict['recieve_writer_time'] = str(datetime.utcnow())
        write_1_file(message_dict)
        write_2_file(message_dict)


async def main():
    await rabbit_broker.connect(
        f"amqp://{RABBITMQ_DEFAULT_USER}:{RABBITMQ_DEFAULT_PASS}@{RABBITMQ_HOST}:{RABBITMQ_PORT}/",
        RABBITMQ_EXCHANGE,
        in_queue=RABBITMQ_IN_QUEUE
    )

    await rabbit_broker.add_consumer(process_message)
    try:
        while True:
            await rabbit_broker.consume()
    except Exception as e:
        print(e)
        await main()


if __name__ == "__main__":
    asyncio.run(main())
