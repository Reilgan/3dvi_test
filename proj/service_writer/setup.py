from setuptools import setup, find_packages

requires = [
    "aio-pika==9.4.1 ",
]

setup(
    name='service_writer',
    version='0,0',
    description='',
    classifiers=[
        'Programming Language :: Python',
    ],
    author='Danila Postnikov',
    author_email='luginich.danil@gmail.com',
    packages=find_packages(),
    include_package_data=True,
    entry_points={
        # 'console_scripts': [
        #     'check-db-connection = app.console:checkDBConnectionCommand'
        # ],
    },
)

