import argparse
import datetime
import json
import logging
import random
import sys
import threading
import requests

connection_value = 0
delay_range = []
lock = threading.Lock()

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
fh = logging.FileHandler('app/requests.log')
fh.setLevel(logging.INFO)
sh = logging.StreamHandler(sys.stdout)
sh.setLevel(logging.INFO)
logger.addHandler(fh)
logger.addHandler(sh)


def req(cv:int):
    global delay_range
    global logger
    with lock:
        for _ in range(cv):
            data = {
                    "request": {
                        "id": random.randint(1, 1000),
                        "delay": random.randint(int(delay_range[0]), int(delay_range[1]))
                    }
            }
            response = requests.post("http://service_receiver:8000/", data=json.dumps(data))
            logger.info(
                json.dumps({
                    "request_time": str(datetime.datetime.utcnow()),
                    "threading_num": threading.get_ident(),
                    "request": data,
                    "response": {
                        "status": response.status_code,
                        'data': response.json()
                    }
                })
            )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--connection_count')
    parser.add_argument('--connection_value')
    parser.add_argument('--delay_range', nargs='+')
    args = parser.parse_args()

    connection_value = int(args.connection_value)
    connection_count = int(args.connection_count)
    connection_values = [
        connection_value//connection_count
        if connection_value % connection_count == 0
        else connection_value % connection_count
        for x in range(connection_count)
    ]

    delay_range = args.delay_range
    threads = [threading.Thread(target=req, args=[connection_values[_]]) for _ in range(connection_count)]

    for thread in threads:
        thread.start()

    for thread in threads:
        thread.join()

    print('finish')