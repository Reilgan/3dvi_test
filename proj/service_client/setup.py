from setuptools import setup, find_packages

requires = [
    "requests==2.31.0",
]

setup(
    name='service_client',
    version='0,0',
    description='',
    classifiers=[
        'Programming Language :: Python',
    ],
    author='Danila Postnikov',
    author_email='luginich.danil@gmail.com',
    packages=find_packages(),
    include_package_data=True,
    install_requires=requires,
    entry_points={
        # 'console_scripts': [
        #     'check-db-connection = app.console:checkDBConnectionCommand'
        # ],
    },
)

