import json

from aio_pika import Message, connect_robust, IncomingMessage
from aio_pika.abc import AbstractRobustConnection, AbstractRobustChannel, AbstractRobustExchange, AbstractRobustQueue, \
    ExchangeType
from pydantic import BaseModel


class RabbitConnection:
    _connection: AbstractRobustConnection | None = None
    _channel: AbstractRobustChannel | None = None
    _exchange: AbstractRobustExchange | None = None
    _in_queue: AbstractRobustQueue | None = None
    _out_queue: AbstractRobustQueue | None = None
    _consumers: list = []
    _in_queue_name: str = None
    _out_queue_name: str = None

    async def disconnect(self) -> None:
        if self._channel and not self._channel.is_closed:
            await self._channel.close()
        if self._connection and not self._connection.is_closed:
            await self._connection.close()
        self._connection = None
        self._channel = None
        self._out_queue = None
        self._out_queue = None

    async def connect(self, url, exchange, in_queue=None, out_queue=None) -> None:
        try:
            self._connection = await connect_robust(url)
            self._channel = await self._connection.channel()
            self._exchange = await self._channel.declare_exchange(
                exchange, ExchangeType.DIRECT,
            )
            if in_queue:
                self._in_queue = await self._channel.declare_queue(in_queue, durable=True)
                await self._in_queue.bind(exchange, in_queue)
                self._in_queue_name = in_queue
            if out_queue:
                self._out_queue = await self._channel.declare_queue(out_queue, durable=True)
                await self._out_queue.bind(exchange, out_queue)
                self._out_queue_name = out_queue

        except Exception as e:
            await self.disconnect()
            raise e

    async def send_messages(
            self,
            messages: list[BaseModel | dict],
    ) -> None:
        for message in messages:
            if isinstance(message, BaseModel):
                message = json.dumps(message.dict()).encode()
            else:
                message = json.dumps(message).encode()

            message = Message(
                body=message,
            )
            await self._exchange.publish(
                message,
                routing_key=self._out_queue_name,
            )

    async def add_consumer(self, consumer):
        self._consumers.append(consumer)

    async def consume(self):
        message: IncomingMessage
        async for message in self._in_queue:
            for consume in self._consumers:
                await consume(message)
