from setuptools import setup, find_packages

requires = [
    "pydantic==2.6.4",
    "aio-pika==9.4.1 ",
]

setup(
    name='rabbit_broker',
    version='0,0',
    description='',
    classifiers=[
        'Programming Language :: Python',
        'Framework :: aio-pika',
    ],
    author='Danila Postnikov',
    author_email='luginich.danil@gmail.com',
    keywords='rabbitmq broker',
    packages=find_packages(),
    include_package_data=True,
    install_requires=requires,
    entry_points={},
)

