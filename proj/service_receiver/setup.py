from setuptools import setup, find_packages

requires = [
    "annotated-types==0.6.0",
    "anyio==4.3.0",
    "click==8.1.7",
    "exceptiongroup==1.2.0",
    "fastapi==0.110.0",
    "h11==0.14.0",
    "idna==3.6",
    "pydantic==2.6.4",
    "pydantic_core==2.16.3",
    "sniffio==1.3.1",
    "starlette==0.36.3",
    "typing_extensions==4.10.0",
    "uvicorn==0.29.0",
    "aio-pika==9.4.1 ",
]

tests_requires = [
    'pytest==7.4.0',
    'pytest-asyncio==0.21.1',
    'httpx==0.24.1'
]

setup(
    name='service_receiver',
    version='0,0',
    description='',
    classifiers=[
        'Programming Language :: Python',
        'Framework :: FastAPI',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: ASGI :: Application',
    ],
    author='Danila Postnikov',
    author_email='luginich.danil@gmail.com',
    keywords='web fastapi',
    packages=find_packages(),
    include_package_data=True,
    extras_require={
        'testing': tests_requires,
    },
    install_requires=requires,
    entry_points={
        # 'console_scripts': [
        #     'check-db-connection = app.console:checkDBConnectionCommand'
        # ],
    },
)

