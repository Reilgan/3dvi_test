from datetime import datetime
from enum import Enum

from pydantic import BaseModel


class Receiver(BaseModel):
    id: int
    delay: int


class ReceiverRequest(BaseModel):
    request: Receiver


class AsyncAnswerType(Enum):
    ok = 'Ok'
    error = 'Error'


class ReceiverResponse(BaseModel):
    asyncAnswer: AsyncAnswerType
