from fastapi import APIRouter

from app.endpoints.receiver_endpoint import receiver_router

router = APIRouter()
router.include_router(receiver_router)



