from datetime import datetime

from fastapi import APIRouter

from app.core import rabbit_broker
from app.schemas.receiver_schema import ReceiverRequest, ReceiverResponse, AsyncAnswerType
receiver_router = APIRouter()


@receiver_router.post("/")
async def receiver(data: ReceiverRequest) -> ReceiverResponse:
    data = data.dict()['request']
    data["recieve_time"] = str(datetime.utcnow())
    await rabbit_broker.send_messages(
        [data]
    )
    return ReceiverResponse(asyncAnswer=AsyncAnswerType.ok)

