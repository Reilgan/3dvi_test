from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from starlette.status import HTTP_422_UNPROCESSABLE_ENTITY
from starlette.types import ASGIApp, Receive, Scope, Send, Message
from app.core import ValidationError


class ValidationErrorMiddleware:

    def __init__(self, app: ASGIApp):
        self.app = app

    async def __call__(self, scope: Scope, receive: Receive, send: Send) -> None:
        if scope['type'] != 'http':
            await self.app(scope, receive, send)
            return

        response_started = False

        async def send_wrapped(message: Message) -> None:
            nonlocal response_started

            if message['type'] == 'http.response.start':
                response_started = True
            await send(message)

        try:
            await self.app(scope, receive, send_wrapped)
        except ValidationError.registered as exc:

            if response_started:
                msg = "Caught handled exception, but response already started."
                raise RuntimeError(msg) from exc

            response = JSONResponse(
                status_code=HTTP_422_UNPROCESSABLE_ENTITY,
                content={"detail": jsonable_encoder(ValidationError.from_exc(exc))},
            )
            await response(scope, receive, send_wrapped)
