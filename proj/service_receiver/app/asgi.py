from contextlib import asynccontextmanager
from fastapi import FastAPI

from app.core import rabbit_broker
from app.settings import debug_mode, RABBITMQ_DEFAULT_USER, RABBITMQ_DEFAULT_PASS, RABBITMQ_HOST, RABBITMQ_PORT, \
    RABBITMQ_EXCHANGE, RABBITMQ_OUT_QUEUE
from app.endpoints import router
from app.middleware import middleware


@asynccontextmanager
async def lifespan(_: FastAPI):
    await rabbit_broker.connect(
        f"amqp://{RABBITMQ_DEFAULT_USER}:{RABBITMQ_DEFAULT_PASS}@{RABBITMQ_HOST}:{RABBITMQ_PORT}/",
        RABBITMQ_EXCHANGE,
        out_queue=RABBITMQ_OUT_QUEUE
    )
    yield
    await rabbit_broker.disconnect()

app = FastAPI(
    debug=debug_mode(),
    middleware=middleware,
    lifespan=lifespan,
)
app.include_router(router)





