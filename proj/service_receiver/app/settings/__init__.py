import os

DEPLOY_ENV = os.getenv('DEPLOY_ENV', 'local')
# middleware
ALLOW_CORS = os.getenv('ALLOW_CORS', None)

#rmq
RABBITMQ_HOST = os.getenv('RABBITMQ_HOST', None)
RABBITMQ_PORT = os.getenv('RABBITMQ_PORT', None)
RABBITMQ_DEFAULT_USER = os.getenv('RABBITMQ_DEFAULT_USER', None)
RABBITMQ_DEFAULT_PASS = os.getenv('RABBITMQ_DEFAULT_PASS', None)
RABBITMQ_OUT_QUEUE = os.getenv('RABBITMQ_QUEUE', "receiver_out")
RABBITMQ_EXCHANGE = os.getenv('RABBITMQ_EXCHANGE', "main")


def debug_mode() -> bool:
    return DEPLOY_ENV == 'local'
