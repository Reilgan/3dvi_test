from typing import Optional


def to_camel_case(string: Optional[str]) -> Optional[str]:
    if not string:
        return string
    result = ''.join(word.capitalize() for word in string.split('_'))
    result = result[0].lower() + result[1:]
    return result
