class ClassPropertyDescriptor:

    def __init__(self, fget, fset=None):
        self.fget = self.check_func(fget)
        self.fset = self.check_func(fset)

    def __get__(self, instance, cls):
        if cls is None:
            cls = type(instance)
        return self.fget.__get__(instance, cls)()

    def __set__(self, instance, value):
        if not self.fset:
            raise AttributeError("can't set attribute")
        cls = type(instance)
        return self.fset.__get__(instance, cls)(value)

    def setter(self, func):
        self.fset = self.check_func(func)
        return self

    @staticmethod
    def check_func(func):
        if not isinstance(func, (classmethod, staticmethod)):
            func = classmethod(func)
        return func


def classproperty(func):
    return ClassPropertyDescriptor(func)
